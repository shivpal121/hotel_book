<?php
$lang['welcome_message'] = 'Hotel Rooms';

//-------featured rooms----------
$lang['single_room'] = 'Single bedroom';
$lang['double_room'] = 'Double bedroom';
$lang['large_room'] = 'Large bedroom';

$lang['f_breakfast'] = 'Incl. breakfast';
$lang['f_balcony'] = 'Private balcony';
$lang['f_sea'] = 'Sea view';
$lang['f_bathe'] = 'Bathroom';
$lang['f_internet'] = 'Free Wi-Fi';
$lang['f_swimming'] = 'Swimming pool';
$lang['f_parking'] = 'Parking';
$lang['f_security'] = 'Security Box';
$lang['book_now'] = 'Book Now';

$lang['gallery'] = 'Gallery';
$lang['find_hotel'] = 'Find Hotel';
