<?php
$lang['home'] = 'Home';
$lang['rooms'] = 'Rooms';
$lang['hotels'] = 'Hotels';
$lang['booking'] = 'Booking';
$lang['gallery'] = 'Gallery';
$lang['contact'] = 'Contact';
$lang['profile'] = 'Profile';
$lang['login'] = 'Login';
$lang['logout'] = 'Logout';