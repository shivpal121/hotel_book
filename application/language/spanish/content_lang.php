<?php
$lang['welcome_message'] = 'Habitaciones de hotel';

//-------featured rooms----------
$lang['single_room'] = 'Habitación individual';
$lang['double_room'] = 'Habitación doble';
$lang['large_room'] = 'Dormitorio grande';

$lang['f_breakfast'] = 'Incl. desayuno';
$lang['f_balcony'] = 'Balcón privado';
$lang['f_sea'] = 'Vista marítima';
$lang['f_bathe'] = 'Baño';
$lang['f_internet'] = 'Wi-Fi gratis';
$lang['f_swimming'] = 'Piscina';
$lang['f_parking'] = 'Estacionamiento';
$lang['f_security'] = 'Caja de seguridad';
$lang['book_now'] = 'Reservar ahora';


$lang['gallery'] = 'Galería';
$lang['find_hotel'] = 'Buscar hotel';