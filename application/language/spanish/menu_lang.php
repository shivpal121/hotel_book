<?php
$lang['home'] = 'Casa';
$lang['rooms'] = 'Habitaciones';
$lang['hotels'] = 'Hoteles';
$lang['booking'] = 'Reserva';
$lang['gallery'] = 'Galería';
$lang['contact'] = 'Contacto';
$lang['profile'] = 'Perfil';
$lang['login'] = 'Iniciar sesión';
$lang['logout'] = 'Cerrar sesión';