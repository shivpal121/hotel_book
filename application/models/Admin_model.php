<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  Admin_Model Extends CI_Model {

	function __construct() {		
	    parent::__construct();
	    $this->load->database();		
	}

	public function get_hotels_list(){
		$this->db->select('h.*');
		$this->db->from('hotels as h');
		//$this->db->where('total_rooms','2');
		$query = $this->db->get();
		$result =  $query->result();
		return $result;
	}
	
	public function get_hotel_detail($hotelid){
		$this->db->select('h.*');
		$this->db->from('hotels as h');
		$this->db->where('h.id',$hotelid);
		$query = $this->db->get();
		$result =  $query->row();
		return $result;
	}
	
	public function update_hotel_detail($userdata,$hotelid){
		$this->db->where('id',$hotelid);
		$this->db->update('hotels',$userdata);
		return 1;
	}
	
	public function insert_hotel($userdata){		
		$this->db->insert('hotels',$userdata);
		return 1;
	}
	
	function get_hotel_room_list($hotelId){		
		$this->db->select('r.*,h.name,h.breakfast,h.internet,h.parking,h.swimming_pool,h.fitness_center,h.bellboys_tips,h.tourist_information,h.security_box');
		$this->db->from('rooms as r');
		$this->db->join('hotels as h','r.hotel_id = h.id','LEFT');
		$this->db->where('h.id',$hotelId);
		$query = $this->db->get();
		$result =  $query->result();
		//print_r($result); die;
		//$this->db->join('room_image as ri','r.id = ri.room_id','RIGHT');
		foreach($result as $key=> $value){
			
			$this->db->where("room_id",$value->id);
			$result2=$this->db->get("room_image")->result();
			$t=array();
			foreach($result2 as $rs)
			{
				$t[]=$rs->image;
			}
			$result[$key]->image=$t;
		}
		
		return $result;
	}
	
	function get_room_detail($roomId){
		$this->db->select('r.*,h.name,h.breakfast,h.internet,h.parking,h.swimming_pool,h.fitness_center,h.bellboys_tips,h.tourist_information,h.security_box');
		$this->db->from('rooms as r');
		$this->db->join('hotels as h','r.hotel_id = h.id','LEFT');
		$this->db->where('r.id',$roomId);
		$query = $this->db->get();
		$result =  $query->row();
		
			$this->db->where("room_id",$roomId);
			$result2=$this->db->get("room_image")->result();
			$t=array();
			foreach($result2 as $rs)
			{
				$t[]=$rs->image;
			}
			$result->image=$t;
				
		return $result;
	}
	
	public function update_room_detail($userdata,$roomid){
		$this->db->where('id',$roomid);
		$this->db->update('rooms',$userdata);
		return 1;
	}
	
	public function insert_room($userdata){		
		$this->db->insert('rooms',$userdata);
		return $this->db->insert_id();
	}
	
	public function insert_room_image($image1,$roomid){
		$userdata['room_id'] = $roomid;
		$userdata['image'] = $image1;
		$this->db->insert('room_image',$userdata);
		return $this->db->insert_id();
	}
	
	public function remove_room_image($img){
		$this->db->where('image',$img);
		$this->db->delete('room_image');
		return 1;
	}
	
	public function get_booking_list(){
		$this->db->select('b.*,h.name,r.room_no');
		$this->db->from('booking as b');
		$this->db->join('hotels as h','b.hotel_id = h.id','LEFT');
		$this->db->join('rooms as r','b.room_id = r.id','LEFT');
		$query = $this->db->get();
		$result =  $query->result();
		return $result;
	}
	
	public function change_booking_status($booking_id,$status){
		$this->db->where('id',$booking_id);
		$userdata['booking_status'] = $status;
		$this->db->update('booking',$userdata);
		return 1;
	}
	
	public function change_room_status($room_id,$status){
		$this->db->where('id',$room_id);
		$userdata['status'] = $status;
		$this->db->update('rooms',$userdata);
		return 1;
	}

}