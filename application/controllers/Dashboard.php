<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller{

	function __construct() {		
	    parent::__construct();
	    $this->load->helper(array('url','form'));
		$this->load->model('admin_model');
	}
	
	
	public function index(){		
		$this->load->view('includes/navbar');
		$this->load->view('dashboard');
		$this->load->view('includes/footer');		
	}	

}