<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hotel extends CI_Controller{

	function __construct() {		
	    parent::__construct();
	    $this->load->helper(array('url','form'));
		$this->load->model('admin_model');
	}

	public function index()
	{		
		$this->load->view('login');	
	}
	
	public function manage_hotel(){
		$data['hotel_list'] = $this->admin_model->get_hotels_list();
		echo json_encode($data);	
	}
	
	public function get_hotel_detail($hotelid){
		$data['hotel_detail'] = $this->admin_model->get_hotel_detail($hotelid);
		echo json_encode($data);	
	}
	
	public function update_hotel(){		
		$data['error'] = array("error"=>'');
		$image = ''; $image1 = '';
		if(!empty($_FILES['hotel_logo'])){
			$ext = pathinfo($_FILES['hotel_logo']['name'],PATHINFO_EXTENSION);
			$image = time().'.'.$ext;
				$config['upload_path']          = './images/logos/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['file_name'] 			= $image;
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);
				$this->upload->initialize($config);

                if ( ! $this->upload->do_upload('hotel_logo'))
                {
                        $data['error'] = array('error' => $this->upload->display_errors());
						echo json_encode($data); die;
                }
                else
                {
                        $data['success'] = array('upload_data' => $this->upload->data());
						$resize['image_library'] = 'gd2';
						$resize['source_image'] = './images/logos/'.$config['file_name'];
						$resize['new_image'] = './images/logos/thumbs/';
						$resize['file_path'] = './images/logos/thumbs/'.$image;
						$resize['create_thumb'] = false;
						$resize['maintain_ratio'] = true;
						$resize['width'] = 200;
						$resize['height']= 150;
						
						$this->load->library('image_lib', $resize);						
						if ( !$this->image_lib->resize()){
							$data['errors'] = array('errors' => $this->image_lib->display_errors('', '')); 
						  }
                }
		}
		if(!empty($_FILES['hotel_image'])){
			$ext = pathinfo($_FILES['hotel_image']['name'],PATHINFO_EXTENSION);
			$image1 = time().'.'.$ext;
				$config['upload_path']          = './images/hotels/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['file_name'] 			= $image1;
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);
				$this->upload->initialize($config);

                if ( ! $this->upload->do_upload('hotel_image'))
                {
                        $data['error'] = array('error' => $this->upload->display_errors());
						echo json_encode($data); die;
                }
                else
                {
                        $data['success'] = array('upload_data' => $this->upload->data());
						$resize['image_library'] = 'gd2';
						$resize['source_image'] = './images/hotels/'.$config['file_name'];
						$resize['new_image'] = './images/hotels/thumbs/';
						$resize['file_path'] = './images/hotels/thumbs/'.$image1;
						$resize['create_thumb'] = false;
						$resize['maintain_ratio'] = true;
						$resize['width'] = 400;
						$resize['height']= 300;
						
						$this->load->library('image_lib', $resize);						
						if ( !$this->image_lib->resize()){
							$data['errors'] = array('errors' => $this->image_lib->display_errors('', '')); 
						  }
                }
		}
		if($_POST){
			$userdata['name'] = $this->input->post('name');
			$userdata['description'] = $this->input->post('description');
			$userdata['address'] = $this->input->post('address');
			$userdata['city'] = $this->input->post('city');
			$userdata['country'] = $this->input->post('country');
			$userdata['internet'] = $this->input->post('internet');
			$userdata['breakfast'] = $this->input->post('breakfast');
			$userdata['parking'] = $this->input->post('parking');
			$userdata['bellboys_tips'] = $this->input->post('bellboytip');
			$userdata['tourist_information'] = $this->input->post('touristinformation');
			$userdata['swimming_pool'] = $this->input->post('swimmingpool');
			$userdata['fitness_center'] = $this->input->post('fitnesscenter');
			$userdata['security_box'] = $this->input->post('securitybox');
			$userdata['check_out_time'] = $this->input->post('checkouttime');
			$userdata['status'] = $this->input->post('status');
			if($image != ''){
			$userdata['logo'] = $image;	
			}
			if($image1 != ''){
			$userdata['hotel_image'] = $image1;	
			}
			
			$hotel_id = $this->input->post('hotel_id');
			
			$rslt = $this->admin_model->update_hotel_detail($userdata,$hotel_id);
			if($rslt){
				$data['success'] = array("success"=>'Updated Successfully');
			}
			
		}
		echo json_encode($data);	
	}
	
	
	
	public function insert_hotel(){		
		$data['error'] = array("error"=>'');
		$data['success'] = array("success"=>'');
		$image = ''; $image1 = '';
		if(!empty($_FILES['hotel_logo'])){
			$ext = pathinfo($_FILES['hotel_logo']['name'],PATHINFO_EXTENSION);
			$image = time().'.'.$ext;
				$config['upload_path']          = './images/logos/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['file_name'] 			= $image;
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);
				$this->upload->initialize($config);

                if ( ! $this->upload->do_upload('hotel_logo'))
                {
                        $data['error'] = array('error' => $this->upload->display_errors());
						echo json_encode($data); die;
                }
                else
                {
                        //$data['success'] = array('upload_data' => $this->upload->data());
						$resize['image_library'] = 'gd2';
						$resize['source_image'] = './images/logos/'.$config['file_name'];
						$resize['new_image'] = './images/logos/thumbs/';
						$resize['file_path'] = './images/logos/thumbs/'.$image;
						$resize['create_thumb'] = false;
						$resize['maintain_ratio'] = true;
						$resize['width'] = 200;
						$resize['height']= 150;
						
						$this->load->library('image_lib', $resize);						
						if ( !$this->image_lib->resize()){
							$data['errors'] = array('errors' => $this->image_lib->display_errors('', '')); 
						  }
                }
		}else{
			$data['error'] = array('error' => 'please select valid logo image');
			echo json_encode($data); die;
		}
		if(!empty($_FILES['hotel_image'])){
			$ext = pathinfo($_FILES['hotel_image']['name'],PATHINFO_EXTENSION);
			$image1 = time().'.'.$ext;
				$config['upload_path']          = './images/hotels/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['file_name'] 			= $image1;
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);
				$this->upload->initialize($config);

                if ( ! $this->upload->do_upload('hotel_image'))
                {
                        $data['error'] = array('error' => $this->upload->display_errors());
						echo json_encode($data); die;
                }
                else
                {
                        //$data['success'] = array('upload_data' => $this->upload->data());
						$resize['image_library'] = 'gd2';
						$resize['source_image'] = './images/hotels/'.$config['file_name'];
						$resize['new_image'] = './images/hotels/thumbs/';
						$resize['file_path'] = './images/hotels/thumbs/'.$image1;
						$resize['create_thumb'] = false;
						$resize['maintain_ratio'] = true;
						$resize['width'] = 400;
						$resize['height']= 300;
						
						$this->load->library('image_lib', $resize);						
						if ( !$this->image_lib->resize()){
							$data['errors'] = array('errors' => $this->image_lib->display_errors('', '')); 
						  }
                }
		}else{
			$data['error'] = array('error' => 'please select valid Hotel image');
			echo json_encode($data); die;
		}
		
		if($_POST){
			$userdata['name'] = $this->input->post('name');
			$userdata['description'] = $this->input->post('description');
			$userdata['address'] = $this->input->post('address');
			$userdata['city'] = $this->input->post('city');
			$userdata['country'] = $this->input->post('country');
			$userdata['internet'] = $this->input->post('internet');
			$userdata['breakfast'] = $this->input->post('breakfast');
			$userdata['parking'] = $this->input->post('parking');
			$userdata['bellboys_tips'] = $this->input->post('bellboytip');
			$userdata['tourist_information'] = $this->input->post('touristinformation');
			$userdata['swimming_pool'] = $this->input->post('swimmingpool');
			$userdata['fitness_center'] = $this->input->post('fitnesscenter');
			$userdata['security_box'] = $this->input->post('securitybox');
			$userdata['check_out_time'] = $this->input->post('checkouttime');
			$userdata['status'] = $this->input->post('status');
			$userdata['total_rooms'] = $this->input->post('total_rooms');
			$userdata['available_rooms'] = $this->input->post('total_rooms');
			if($image != ''){
			$userdata['logo'] = $image;	
			}
			if($image1 != ''){
			$userdata['hotel_image'] = $image1;	
			}
						
			
			$rslt = $this->admin_model->insert_hotel($userdata);
			if($rslt){
				$data['success'] = array("success"=>'Inserted Successfully');
			}
			
		}
		echo json_encode($data);	
	}
	
	public function get_hotel_room_list($hotelId){
		$data['hotel_room_list'] = $this->admin_model->get_hotel_room_list($hotelId);				
		echo json_encode($data);
	}
	
	public function get_room_detail($roomid){
		$data['room_detail'] = $this->admin_model->get_room_detail($roomid);
		echo json_encode($data);	
	}
	
	
	public function update_room(){		
		$data['error'] = array("error"=>'');
		$image = ''; $image1 = '';
		$data['detail'] = $_FILES;
		$rslt = $this->input->post('room_id');
		if(!empty($_FILES['image2'])){
			$ext = pathinfo($_FILES['image2']['name'],PATHINFO_EXTENSION);
			$image2 = "2".time().'.'.$ext;
				$config['upload_path']          = './images/rooms/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['file_name'] 			= $image2;
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);
				$this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image2'))
                {
                        $data['error'] = array('error' => $this->upload->display_errors());
						echo json_encode($data); die;
                }
                else
                {
                        //$data['success'] = array('upload_data' => $this->upload->data());
						$resize2['image_library'] = 'gd2';
						$resize2['source_image'] = './images/rooms/'.$image2;
						$resize2['new_image'] = './images/rooms/thumbs/';
						$resize2['file_path'] = './images/rooms/thumbs/'.$image2;
						$resize2['create_thumb'] = false;
						$resize2['maintain_ratio'] = true;
						$resize2['width'] = 400;
						$resize2['height']= 300;
						
						$this->load->library('image_lib', $resize2);						
						if ( !$this->image_lib->resize()){
							$data['errors'] = array('errors' => $this->image_lib->display_errors('', '')); 
						  }
						$this->admin_model->insert_room_image($image2,$rslt);
                }
		}
		if(!empty($_FILES['image3'])){
			$ext = pathinfo($_FILES['image3']['name'],PATHINFO_EXTENSION);
			$image3 = "3".time().'.'.$ext;
				$config['upload_path']          = './images/rooms/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['file_name'] 			= $image3;
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);
				$this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image3'))
                {
                        $data['error'] = array('error' => $this->upload->display_errors());
						echo json_encode($data); die;
                }
                else
                {
                        //$data['success'] = array('upload_data' => $this->upload->data());
						$resize3['image_library'] = 'gd2';
						$resize3['source_image'] = './images/rooms/'.$image3;
						$resize3['new_image'] = './images/rooms/thumbs/';
						$resize3['file_path'] = './images/rooms/thumbs/'.$image3;
						$resize3['create_thumb'] = false;
						$resize3['maintain_ratio'] = true;
						$resize3['width'] = 400;
						$resize3['height']= 300;
						
						$this->load->library('image_lib', $resize3);						
						if ( !$this->image_lib->resize()){
							$data['errors'] = array('errors' => $this->image_lib->display_errors('', '')); 
						  }
						$this->admin_model->insert_room_image($image3,$rslt);  
                }
		}
		if(!empty($_FILES['image4'])){
			$ext = pathinfo($_FILES['image4']['name'],PATHINFO_EXTENSION);
			$image4 = "4".time().'.'.$ext;
				$config['upload_path']          = './images/rooms/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['file_name'] 			= $image4;
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);
				$this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image4'))
                {
                        $data['error'] = array('error' => $this->upload->display_errors());
						echo json_encode($data); die;
                }
                else
                {
                        //$data['success'] = array('upload_data' => $this->upload->data());
						$resize4['image_library'] = 'gd2';
						$resize4['source_image'] = './images/rooms/'.$image4;
						$resize4['new_image'] = './images/rooms/thumbs/';
						$resize4['file_path'] = './images/rooms/thumbs/'.$image4;
						$resize4['create_thumb'] = false;
						$resize4['maintain_ratio'] = true;
						$resize4['width'] = 400;
						$resize4['height']= 300;
						
						$this->load->library('image_lib', $resize4);						
						if ( !$this->image_lib->resize()){
							$data['errors'] = array('errors' => $this->image_lib->display_errors('', '')); 
						  }
						$this->admin_model->insert_room_image($image4,$rslt);  
                }
		}
		if(!empty($_FILES['image5'])){
			$ext = pathinfo($_FILES['image5']['name'],PATHINFO_EXTENSION);
			$image5 = "5".time().'.'.$ext;
				$config['upload_path']          = './images/rooms/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['file_name'] 			= $image5;
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);
				$this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image5'))
                {
                        $data['error'] = array('error' => $this->upload->display_errors());
						echo json_encode($data); die;
                }
                else
                {
                        //$data['success'] = array('upload_data' => $this->upload->data());
						$resize5['image_library'] = 'gd2';
						$resize5['source_image'] = './images/rooms/'.$image5;
						$resize5['new_image'] = './images/rooms/thumbs/';
						$resize5['file_path'] = './images/rooms/thumbs/'.$image5;
						$resize5['create_thumb'] = false;
						$resize5['maintain_ratio'] = true;
						$resize5['width'] = 400;
						$resize5['height']= 300;
						
						$this->load->library('image_lib', $resize5);						
						if ( !$this->image_lib->resize()){
							$data['errors'] = array('errors' => $this->image_lib->display_errors('', '')); 
						  }
						$this->admin_model->insert_room_image($image5,$rslt);  
                }
		}
		if(!empty($_FILES['image6'])){
			$ext = pathinfo($_FILES['image6']['name'],PATHINFO_EXTENSION);
			$image6 = "6".time().'.'.$ext;
				$config['upload_path']          = './images/rooms/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['file_name'] 			= $image6;
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);
				$this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image6'))
                {
                        $data['error'] = array('error' => $this->upload->display_errors());
						echo json_encode($data); die;
                }
                else
                {
                        //$data['success'] = array('upload_data' => $this->upload->data());
						$resize6['image_library'] = 'gd2';
						$resize6['source_image'] = './images/rooms/'.$image6;
						$resize6['new_image'] = './images/rooms/thumbs/';
						$resize6['file_path'] = './images/rooms/thumbs/'.$image6;
						$resize6['create_thumb'] = false;
						$resize6['maintain_ratio'] = true;
						$resize6['width'] = 400;
						$resize6['height']= 300;
						
						$this->load->library('image_lib', $resize6);						
						if ( !$this->image_lib->resize()){
							$data['errors'] = array('errors' => $this->image_lib->display_errors('', '')); 
						  }
						$this->admin_model->insert_room_image($image6,$rslt);  
                }
		}
		if($_POST){
			$userdata['room_title'] = $this->input->post('title');
			$userdata['room_desc'] = $this->input->post('description');
			$userdata['price'] = $this->input->post('price');
			$userdata['bed'] = $this->input->post('bed');
			$userdata['tv'] = $this->input->post('tv');
			$userdata['ac'] = $this->input->post('ac');
			$userdata['balcony'] = $this->input->post('balcony');
			$userdata['sea_view'] = $this->input->post('sea_view');
			$userdata['lat_bathe'] = $this->input->post('lat_bathe');
			$userdata['status'] = $this->input->post('status');
												
			$room_id = $this->input->post('room_id');
			
			$rslt = $this->admin_model->update_room_detail($userdata,$room_id);
			if($rslt){
				$data['success'] = array("success"=>'Updated Successfully');
			}
			
		}
		echo json_encode($data);	
	}
	
	public function insert_room(){
		$data['detail'] = $_FILES;
		$data['error'] = array("error"=>'');
		$data['success'] = array("success"=>'');				
		
		if(!empty($_FILES['image1'])){
			$ext = pathinfo($_FILES['image1']['name'],PATHINFO_EXTENSION);
			$image1 = "1".time().'.'.$ext;
				$config['upload_path']          = './images/rooms/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['file_name'] 			= $image1;
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);
				$this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image1'))
                {
                        $data['error'] = array('error' => $this->upload->display_errors());
						echo json_encode($data); die;
                }
                else
                {
                        //$data['success'] = array('upload_data' => $this->upload->data());
						$resize['image_library'] = 'gd2';
						$resize['source_image'] = './images/rooms/'.$config['file_name'];
						$resize['new_image'] = './images/rooms/thumbs/';
						$resize['file_path'] = './images/rooms/thumbs/'.$image1;
						$resize['create_thumb'] = false;
						$resize['maintain_ratio'] = true;
						$resize['width'] = 400;
						$resize['height']= 300;
						
						$this->load->library('image_lib', $resize);						
						if ( !$this->image_lib->resize()){
							$data['errors'] = array('errors' => $this->image_lib->display_errors('', '')); 
						  }
						$image1_succ = 1;  						
                }
		}else{
			$data['error'] = array('error' => 'please select Hotel image1');
			//echo json_encode($data); die;
		}
		if(!empty($_FILES['image2'])){
			$ext = pathinfo($_FILES['image2']['name'],PATHINFO_EXTENSION);
			$image2 = "2".time().'.'.$ext;
				$config['upload_path']          = './images/rooms/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['file_name'] 			= $image2;
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);
				$this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image2'))
                {
                        $data['error'] = array('error' => $this->upload->display_errors());
						echo json_encode($data); die;
                }
                else
                {
                        //$data['success'] = array('upload_data' => $this->upload->data());
						$resize2['image_library'] = 'gd2';
						$resize2['source_image'] = './images/rooms/'.$image2;
						$resize2['new_image'] = './images/rooms/thumbs/';
						$resize2['file_path'] = './images/rooms/thumbs/'.$image2;
						$resize2['create_thumb'] = false;
						$resize2['maintain_ratio'] = true;
						$resize2['width'] = 400;
						$resize2['height']= 300;
						
						$this->load->library('image_lib', $resize2);						
						if ( !$this->image_lib->resize()){
							$data['errors'] = array('errors' => $this->image_lib->display_errors('', '')); 
						  }
						$image2_succ = 1; 
                }
		}
		if(!empty($_FILES['image3'])){
			$ext = pathinfo($_FILES['image3']['name'],PATHINFO_EXTENSION);
			$image3 = "3".time().'.'.$ext;
				$config['upload_path']          = './images/rooms/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['file_name'] 			= $image3;
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);
				$this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image3'))
                {
                        $data['error'] = array('error' => $this->upload->display_errors());
						echo json_encode($data); die;
                }
                else
                {
                        //$data['success'] = array('upload_data' => $this->upload->data());
						$resize3['image_library'] = 'gd2';
						$resize3['source_image'] = './images/rooms/'.$image3;
						$resize3['new_image'] = './images/rooms/thumbs/';
						$resize3['file_path'] = './images/rooms/thumbs/'.$image3;
						$resize3['create_thumb'] = false;
						$resize3['maintain_ratio'] = true;
						$resize3['width'] = 400;
						$resize3['height']= 300;
						
						$this->load->library('image_lib', $resize3);						
						if ( !$this->image_lib->resize()){
							$data['errors'] = array('errors' => $this->image_lib->display_errors('', '')); 
						  }
						$image3_succ = 1; 
                }
		}
		if(!empty($_FILES['image4'])){
			$ext = pathinfo($_FILES['image4']['name'],PATHINFO_EXTENSION);
			$image4 = "4".time().'.'.$ext;
				$config['upload_path']          = './images/rooms/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['file_name'] 			= $image4;
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);
				$this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image4'))
                {
                        $data['error'] = array('error' => $this->upload->display_errors());
						echo json_encode($data); die;
                }
                else
                {
                        //$data['success'] = array('upload_data' => $this->upload->data());
						$resize4['image_library'] = 'gd2';
						$resize4['source_image'] = './images/rooms/'.$image4;
						$resize4['new_image'] = './images/rooms/thumbs/';
						$resize4['file_path'] = './images/rooms/thumbs/'.$image4;
						$resize4['create_thumb'] = false;
						$resize4['maintain_ratio'] = true;
						$resize4['width'] = 400;
						$resize4['height']= 300;
						
						$this->load->library('image_lib', $resize4);						
						if ( !$this->image_lib->resize()){
							$data['errors'] = array('errors' => $this->image_lib->display_errors('', '')); 
						  }
						$image4_succ = 1;   
                }
		}
		if(!empty($_FILES['image5'])){
			$ext = pathinfo($_FILES['image5']['name'],PATHINFO_EXTENSION);
			$image5 = "5".time().'.'.$ext;
				$config['upload_path']          = './images/rooms/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['file_name'] 			= $image5;
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);
				$this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image5'))
                {
                        $data['error'] = array('error' => $this->upload->display_errors());
						echo json_encode($data); die;
                }
                else
                {
                        //$data['success'] = array('upload_data' => $this->upload->data());
						$resize5['image_library'] = 'gd2';
						$resize5['source_image'] = './images/rooms/'.$image5;
						$resize5['new_image'] = './images/rooms/thumbs/';
						$resize5['file_path'] = './images/rooms/thumbs/'.$image5;
						$resize5['create_thumb'] = false;
						$resize5['maintain_ratio'] = true;
						$resize5['width'] = 400;
						$resize5['height']= 300;
						
						$this->load->library('image_lib', $resize5);						
						if ( !$this->image_lib->resize()){
							$data['errors'] = array('errors' => $this->image_lib->display_errors('', '')); 
						  }
						$image5_succ = 1; 
                }
		}
		if(!empty($_FILES['image6'])){
			$ext = pathinfo($_FILES['image6']['name'],PATHINFO_EXTENSION);
			$image6 = "6".time().'.'.$ext;
				$config['upload_path']          = './images/rooms/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
				$config['file_name'] 			= $image6;
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);
				$this->upload->initialize($config);

                if ( ! $this->upload->do_upload('image6'))
                {
                        $data['error'] = array('error' => $this->upload->display_errors());
						echo json_encode($data); die;
                }
                else
                {
                        //$data['success'] = array('upload_data' => $this->upload->data());
						$resize6['image_library'] = 'gd2';
						$resize6['source_image'] = './images/rooms/'.$image6;
						$resize6['new_image'] = './images/rooms/thumbs/';
						$resize6['file_path'] = './images/rooms/thumbs/'.$image6;
						$resize6['create_thumb'] = false;
						$resize6['maintain_ratio'] = true;
						$resize6['width'] = 400;
						$resize6['height']= 300;
						
						$this->load->library('image_lib', $resize6);						
						if ( !$this->image_lib->resize()){
							$data['errors'] = array('errors' => $this->image_lib->display_errors('', '')); 
						  }
						$image6_succ = 1;   
                }
		}
		
		if($_POST){
			$userdata['hotel_id'] = $this->input->post('hotel_id');
			$userdata['room_no'] = $this->input->post('room_no');
			$userdata['room_title'] = $this->input->post('title');
			$userdata['room_desc'] = $this->input->post('description');
			$userdata['price'] = $this->input->post('price');
			$userdata['bed'] = $this->input->post('bed');
			$userdata['tv'] = $this->input->post('tv');
			$userdata['ac'] = $this->input->post('ac');
			$userdata['balcony'] = $this->input->post('balcony');
			$userdata['sea_view'] = $this->input->post('sea_view');
			$userdata['lat_bathe'] = $this->input->post('lat_bathe');
			$userdata['status'] = $this->input->post('status');
						
			
			$rslt = $this->admin_model->insert_room($userdata);
			if($rslt){
				$data['success'] = array("success"=>'Inserted Successfully');
			}
			
		}
		
		if(isset($image1_succ) ){
			$this->admin_model->insert_room_image($image1,$rslt);
		}
		if(isset($image2_succ) ){
			$this->admin_model->insert_room_image($image2,$rslt);
		}
		if(isset($image3_succ) ){
			$this->admin_model->insert_room_image($image3,$rslt);
		}
		if(isset($image4_succ) ){
			$this->admin_model->insert_room_image($image4,$rslt);
		}
		if(isset($image5_succ) ){
			$this->admin_model->insert_room_image($image5,$rslt);
		}
		if(isset($image6_succ) ){
			$this->admin_model->insert_room_image($image6,$rslt);
		}
		
		
		echo json_encode($data);	
	}
	
	public function remove_image(){
		$dataa = json_decode(file_get_contents("php://input"));
		$data['success'] = $this->admin_model->remove_room_image($dataa->image);
		echo json_encode($data);
	}
	
	public function manage_booking(){
		$data['booking_list'] = $this->admin_model->get_booking_list();
		echo json_encode($data);	
	}
	
	public function change_booking_status(){
		$dataa = json_decode(file_get_contents("php://input"));
		$status = $dataa->status;
		$booking_id = $dataa->booking_id;
		$room_id = $dataa->room_id;
		if($status == '0'){
			$new_status = '1';
			$this->admin_model->change_room_status($room_id,$new_status);
		}else{
			$new_status = '0';
			$this->admin_model->change_room_status($room_id,$new_status);
		}
		$data['success'] = $this->admin_model->change_booking_status($booking_id,$new_status);
		echo json_encode($data);
	}
	
}