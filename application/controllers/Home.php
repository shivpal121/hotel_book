<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller{

	function __construct() {		
	    parent::__construct();
	    $this->load->helper(array('url','form'));
		$this->load->model('admin_model');
	}

	public function index()
	{		
		$this->load->view('login');	
	}
	
	public function check_session(){
		if($this->session->userdata('userdata') && $this->session->userdata['userdata']['logged_in'] == 'true'){
			$data['isLogin'] = 1;
		}else{
			$data['isLogin'] = 0;
		}
		echo json_encode($data);
	}
	
	public function check_login()
	{		
		if(!empty($_POST)){
			$username = $this->input->post('username');
			$pass = $this->input->post('password');
			
			if($username == 'admin' && $pass == 'admin'){
				$sess_array = array(
									"username" => $username,
									"logged_in" =>true,
									"user_type" =>1);
				$this->session->set_userdata('userdata',$sess_array);
				redirect('dashboard');
			}else{
				$res['error'] = 1;
				$res['success'] = 0;
				$res['msg'] = "Invalid Username or Password";
				$this->session->set_flashdata('item',$res);
				redirect('home');
			}
		}
	}
	
	public function logout(){
			$this->session->unset_userdata('userdata');
			$this->session->sess_destroy();
			redirect('home');
	}
}