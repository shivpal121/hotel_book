<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class LanguageSwitcher extends CI_Controller
{
    public function __construct() {
        parent::__construct();     
    }
 
    function switchLang($language = "",$uri) {
        
        $language = ($language != "") ? $language : "english";
        $this->session->set_userdata('site_lang', $language);
		//echo $_SERVER['HTTP_REFERER'];die;
        
        redirect($_SERVER['HTTP_REFERER'].'#!/'.$uri);
        
    }
}