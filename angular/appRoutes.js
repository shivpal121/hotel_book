angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

	$routeProvider

		// home page
		.when('/homes', {
			templateUrl: 'http://localhost/antads/templates/home.html',
			controller: 'MainController'
		})

		.when('/aboutus', {
			templateUrl: 'http://localhost/angular/aboutus.html',
			controller: 'AboutusController'
		})

		.when('/contactus', {
			templateUrl: 'http://localhost/angular/contactus.html',
			controller: 'ContactusController'	
		})

		.when('/login', {
			templateUrl: 'http://localhost/angular/login.html',
			controller: 'LoginController'	
		});

	//$locationProvider.html5Mode(true);

}]);