var site_url = "http://localhost/antad_admin/";

var app = angular.module("antadAdminApp", ["ngRoute","datatables"]);

//app.config(['$locationProvider', function($locationProvider){
//	$locationProvider.html5Mode(true);
//}]);

app.config(function($routeProvider) {
    $routeProvider
	.when("/", {
        templateUrl : "templates/dashboard.html"
		
    })
    .when("/add_user", {
        templateUrl : "templates/add_user.html"
		
    })
    .when("/manage_user", {
        templateUrl : "templates/manage_user.html"
		
    })
    .when("/add_hotel", {
        templateUrl : "templates/add_hotel.html"
		
    })
    .when("/manage_hotel", {
        templateUrl : "templates/manage_hotel.html"
		
    })
    .when("/hotel_detail/:hotelId", {
        templateUrl : "templates/hotel_detail.html"
		
    })
    .when("/add_room", {
        templateUrl : "templates/add_room.html"
		
    })
    .when("/hotel_room_list/:hotelId", {
        templateUrl : "templates/hotel_room_list.html"
		
    })
    .when("/room_detail/:roomId", {
        templateUrl : "templates/room_detail.html"
		
    })
    .when("/add_banner", {
        templateUrl : "templates/add_banner.html"
		
    })
    .when("/manage_banner", {
        templateUrl : "templates/manage_banner.html"
		
    })
    .when("/manage_booking", {
        templateUrl : "templates/manage_booking.html"
		
    })
    .when("/update_profile", {
        templateUrl : "templates/update_profile.html"
		
    })
    .when("/change_password", {
        templateUrl : "templates/change_password.html"
		
    });
	
});


app.controller("hotelCtrl", function($scope,$http,DTOptionsBuilder) {
        
        // DataTables configurable options
         $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDisplayLength(10)
        .withOption('bLengthChange', false);
    
        $scope.hotelList = {};
        
		$http.post(site_url+"hotel/manage_hotel")
			.then(function(response) {
				//alert(response.data);				
				$scope.hotelList = response.data.hotel_list;
                $scope.site_url = site_url;
				console.log($scope.hotelList);						
			});
        
    
    $http.get(site_url+"home/check_session")
    .then(function(response) {
		$scope.authenticate = response.data.isLogin;
			if($scope.authenticate === 0){
				window.location.href = '#!/';
			}
    });
	
	
});


app.controller('hotelDetailCtrl', function($scope,$http,$routeParams) {	
    $scope.formdata = {};
		var id = $routeParams.hotelId;		
		$http.get(site_url+"hotel/get_hotel_detail/"+id)
		.then(function(response) {
			console.log(response.data);
			$scope.hotelDetail = response.data.hotel_detail;
            $scope.form.hotel_name = $scope.hotelDetail.name;
            $scope.form.hotel_decsription = $scope.hotelDetail.description;
            $scope.form.hotel_address = $scope.hotelDetail.address;
            $scope.form.hotel_city = $scope.hotelDetail.city;
            $scope.form.hotel_country = $scope.hotelDetail.country;
            $scope.form.totalrooms = $scope.hotelDetail.total_rooms;
            $scope.form.availablerooms = $scope.hotelDetail.available_rooms;
            $scope.form.internet = $scope.hotelDetail.internet;
            $scope.form.breakfast = $scope.hotelDetail.breakfast;                    
            $scope.form.parking = $scope.hotelDetail.parking;
            $scope.form.bellboytip = $scope.hotelDetail.bellboys_tips;
            $scope.form.touristinformation = $scope.hotelDetail.tourist_information;
            $scope.form.swimmingpool = $scope.hotelDetail.swimming_pool;
            $scope.form.fitnesscenter = $scope.hotelDetail.fitness_center;
            $scope.form.securitybox = $scope.hotelDetail.security_box;
            $scope.form.checkouttime = $scope.hotelDetail.check_out_time;
            $scope.form.status = $scope.hotelDetail.status;
		});
		
		$http.get(site_url+"home/check_session")
		.then(function(response) {
			//console.log(response.data);
			$scope.authenticate = response.data.isLogin;
				if($scope.authenticate === 0){
					window.location.href = '#!/';
				}
		});
        
            $scope.form = [];
            $scope.files = [];            

            $scope.submit = function() {
                //alert($scope.form.bellboytip);
	      	$scope.form.hotel_logo = $scope.files[0];
            $scope.form.hotel_image = $scope.files1[0];
	      	$http({
			  method  : 'POST',
			  url     : site_url+'hotel/update_hotel',
			  processData: false,
			  transformRequest: function (data) {
			      var formData = new FormData();
			      formData.append("hotel_logo", $scope.form.hotel_logo);
                  formData.append("hotel_image", $scope.form.hotel_image);
                  formData.append("name", $scope.form.hotel_name);
                  formData.append("description", $scope.form.hotel_decsription);
                  formData.append("address", $scope.form.hotel_address);
                  formData.append("city", $scope.form.hotel_city);
                  formData.append("country", $scope.form.hotel_country);
                  formData.append("internet", $scope.form.internet);
                  formData.append("breakfast", $scope.form.breakfast);                                     
                  formData.append("parking", $scope.form.parking);
                  formData.append("bellboytip", $scope.form.bellboytip);
                  formData.append("touristinformation", $scope.form.touristinformation);
                  formData.append("swimmingpool", $scope.form.swimmingpool);
                  formData.append("fitnesscenter", $scope.form.fitnesscenter);
                  formData.append("securitybox", $scope.form.securitybox);
                  formData.append("checkouttime", $scope.form.checkouttime);
                  formData.append("status", $scope.form.status);
                  formData.append("hotel_id", $scope.hotelDetail.id);
			      return formData;  
			  },  
			  data : $scope.form,
			  headers: {
			         'Content-Type': undefined
			  }
		   }).then(function(response){
		       console.log(response);
               if(response.data.error.error !== ''){
                alert(response.data.error.error);
               }
               if(response.data.success.success !== ''){
                alert(response.data.success.success);
                location.reload();
               }
		   });

	      };
          
          
           $scope.uploadedFile = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source = event.target.result;
		      $scope.$apply(function($scope) {
		        $scope.files = element.files;
		      });
		    };
                    reader.readAsDataURL(element.files[0]);
		  };
          
          $scope.files1 = [];
          
          $scope.uploadedFile1 = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source1 = event.target.result;
		      $scope.$apply(function($scope) {
		        $scope.files1 = element.files;
		      });
		    };
                    reader.readAsDataURL(element.files[0]);
		  };
});


app.controller('addHotelCtrl', function($scope,$http) {	                  
		
		$http.get(site_url+"home/check_session")
		.then(function(response) {
			//console.log(response.data);
			$scope.authenticate = response.data.isLogin;
				if($scope.authenticate === 0){
					window.location.href = '#!/';
				}
            $scope.form.internet = '1';
            $scope.form.breakfast = '1';                    
            $scope.form.parking = '1';
            $scope.form.bellboytip = '1';
            $scope.form.touristinformation = '1';
            $scope.form.swimmingpool = '1';
            $scope.form.fitnesscenter = '1';
            $scope.form.securitybox = '1';
            $scope.form.checkouttime = '24 hrs';
            $scope.form.status = '1';
		});
        
            $scope.form = [];
            $scope.files = [];            

            $scope.submit = function() {                                               
                //alert($scope.form.internet);
	      	$scope.form.hotel_logo = $scope.files[0];
            $scope.form.hotel_image = $scope.files1[0];
	      	$http({
			  method  : 'POST',
			  url     : site_url+'hotel/insert_hotel',
			  processData: false,
			  transformRequest: function (data) {
			      var formData = new FormData();
			      formData.append("hotel_logo", $scope.form.hotel_logo);
                  formData.append("hotel_image", $scope.form.hotel_image);
                  formData.append("name", $scope.form.hotel_name);
                  formData.append("description", $scope.form.hotel_decsription);
                  formData.append("address", $scope.form.hotel_address);
                  formData.append("city", $scope.form.hotel_city);
                  formData.append("country", $scope.form.hotel_country);
                  formData.append("internet", $scope.form.internet);
                  formData.append("breakfast", $scope.form.breakfast);                                     
                  formData.append("parking", $scope.form.parking);
                  formData.append("bellboytip", $scope.form.bellboytip);
                  formData.append("touristinformation", $scope.form.touristinformation);
                  formData.append("swimmingpool", $scope.form.swimmingpool);
                  formData.append("fitnesscenter", $scope.form.fitnesscenter);
                  formData.append("securitybox", $scope.form.securitybox);
                  formData.append("checkouttime", $scope.form.checkouttime);
                  formData.append("status", $scope.form.status);
                  formData.append("total_rooms", $scope.form.totalrooms);
                  
			      return formData;  
			  },  
			  data : $scope.form,
			  headers: {
			         'Content-Type': undefined
			  }
		   }).then(function(response){
		       console.log(response);
               if(response.data.error.error !== ''){
                alert(response.data.error.error);
               }
               if(response.data.success.success !== ''){
                alert(response.data.success.success);
                location.reload();
               }
		   });

	      };
          
          
           $scope.uploadedFile = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source = event.target.result;
		      $scope.$apply(function($scope) {
		        $scope.files = element.files;
		      });
		    };
                    reader.readAsDataURL(element.files[0]);
		  };
          
          $scope.files1 = [];
          
          $scope.uploadedFile1 = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source1 = event.target.result;
		      $scope.$apply(function($scope) {
		        $scope.files1 = element.files;
		      });
		    };
                    reader.readAsDataURL(element.files[0]);
		  };
});

app.controller('hotelRoomListCtrl', function($scope,$http,$routeParams) {	
    
		var id = $routeParams.hotelId;		
		$http.get(site_url+"hotel/get_hotel_room_list/"+id)
		.then(function(response) {
			console.log(response.data);
			$scope.hotelRoomList = response.data.hotel_room_list;
			$scope.hotelName = response.data.hotel_room_list[0].name;
		});
		
		$http.get(site_url+"home/check_session")
		.then(function(response) {
			//console.log(response.data);
			$scope.authenticate = response.data.isLogin;
				if($scope.authenticate === 0){
					window.location.href = '#!/';
				}
		});
			   
});


app.controller('roomDetailCtrl', function($scope,$http,$routeParams) {	
    $scope.formdata = {};
		var id = $routeParams.roomId;		
		$http.get(site_url+"hotel/get_room_detail/"+id)
		.then(function(response) {
			console.log(response.data);
			$scope.roomDetail = response.data.room_detail;
            $scope.form.title = $scope.roomDetail.room_title;
            $scope.form.room_decsription = $scope.roomDetail.room_desc;
            $scope.form.room_price = $scope.roomDetail.price;
            $scope.form.bed = $scope.roomDetail.bed;
            $scope.form.tv = $scope.roomDetail.tv;
            $scope.form.ac = $scope.roomDetail.ac;
            $scope.form.balcony = $scope.roomDetail.balcony;
            $scope.form.sea_view = $scope.roomDetail.sea_view;
            $scope.form.lat_bathe = $scope.roomDetail.lat_bathe;
            $scope.form.status = $scope.roomDetail.status;
		});
		
		$http.get(site_url+"home/check_session")
		.then(function(response) {
			//console.log(response.data);
			$scope.authenticate = response.data.isLogin;
				if($scope.authenticate === 0){
					window.location.href = '#!/';
				}
		});
        
            $scope.form = [];
            $scope.files = [];            

            $scope.submit = function() {
               // alert($scope.files2[0]);
                console.log($scope.files2[0]); 
	      	$scope.form.image = $scope.files[0];
            $scope.form.image2 = $scope.files2[0];
            $scope.form.image3 = $scope.files3[0];
            $scope.form.image4 = $scope.files4[0];
            $scope.form.image5 = $scope.files5[0];
            $scope.form.image6 = $scope.files6[0];
	      	$http({
			  method  : 'POST',
			  url     : site_url+'hotel/update_room',
			  processData: false,
			  transformRequest: function (data) {
			      var formData = new FormData();			      
                  formData.append("image1", $scope.form.image);
                  formData.append("image2", $scope.form.image2);
                  formData.append("image3", $scope.form.image3);
                  formData.append("image4", $scope.form.image4);
                  formData.append("image5", $scope.form.image5);
                  formData.append("image6", $scope.form.image6);
                  formData.append("title", $scope.form.title);
                  formData.append("description", $scope.form.room_decsription);
                  formData.append("price", $scope.form.room_price);
                  formData.append("bed", $scope.form.bed);
                  formData.append("tv", $scope.form.tv);
                  formData.append("ac", $scope.form.ac);
                  formData.append("balcony", $scope.form.balcony);                                     
                  formData.append("sea_view", $scope.form.sea_view);
                  formData.append("lat_bathe", $scope.form.lat_bathe);
                  formData.append("status", $scope.form.status);                  
                  formData.append("room_id", $scope.roomDetail.id);
			      return formData;  
			  },  
			  data : $scope.form,
			  headers: {
			         'Content-Type': undefined
			  }
		   }).then(function(response){
		       console.log(response);
               if(response.data.error.error !== ''){
                alert(response.data.error.error);
               }
               if(response.data.success.success !== ''){
                alert(response.data.success.success);
                location.reload();
               }
		   });

	      };
          
          
           $scope.uploadedFile = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source = event.target.result;
		      $scope.$apply(function($scope) {
		        $scope.files = element.files;
		      });
		    };
                    reader.readAsDataURL(element.files[0]);
		  };
          
          $scope.files1 = [];
          
          $scope.uploadedFile1 = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source1 = event.target.result;
		      $scope.$apply(function($scope) {
		        $scope.files1 = element.files;                
		      });
		    };
                    reader.readAsDataURL(element.files[0]);
		  };
          $scope.files2 = [];
          $scope.uploadedFile2 = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source2 = event.target.result;
		      $scope.$apply(function($scope) {
		        $scope.files2 = element.files;
                console.log($scope.files2);
		      });
		    };
                    reader.readAsDataURL(element.files[0]);
		  };
          $scope.files3 = [];
          $scope.uploadedFile3 = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source3 = event.target.result;
		      $scope.$apply(function($scope) {
		        $scope.files3 = element.files;
		      });
		    };
                    reader.readAsDataURL(element.files[0]);
		  };
          $scope.files4 = [];
          $scope.uploadedFile4 = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source4 = event.target.result;
		      $scope.$apply(function($scope) {
		        $scope.files4 = element.files;
		      });
		    };
                    reader.readAsDataURL(element.files[0]);
		  };
          $scope.files5 = [];
          $scope.uploadedFile5 = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source5 = event.target.result;
		      $scope.$apply(function($scope) {
		        $scope.files5 = element.files;
		      });
		    };
                    reader.readAsDataURL(element.files[0]);
		  };
          $scope.files6 = [];
          $scope.uploadedFile6 = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source6 = event.target.result;
		      $scope.$apply(function($scope) {
		        $scope.files6 = element.files;
		      });
		    };
                    reader.readAsDataURL(element.files[0]);
		  };
          
        $scope.remove_image = function(img){
          //alert(img);
            if (confirm("Are you sure want to delete this image!") === true) {
                    //alert(img);
        
                        $http.post(site_url+"hotel/remove_image",{
                        'image':img                        
                        })
                        .then(function(response) {
                            console.log(response.data);				
                            location.reload();  	
                        });
   
                } else {
                    
                }
        };
        
        
});


app.controller('addRoomCtrl', function($scope,$http) {	
    $scope.formdata = {};
	
		$http.get(site_url+"hotel/manage_hotel/")
		.then(function(response) {
			console.log(response.data);
			$scope.hotelList = response.data.hotel_list;
            $scope.hotelId = 1;	
            $scope.form.bed = '1';
            $scope.form.tv = '1';
            $scope.form.ac = '1';
            $scope.form.balcony = '1';
            $scope.form.sea_view = '1';
            $scope.form.lat_bathe = '1';
            $scope.form.status = '1';
		});
		
		$http.get(site_url+"home/check_session")
		.then(function(response) {
			//console.log(response.data);
			$scope.authenticate = response.data.isLogin;
				if($scope.authenticate === 0){
					window.location.href = '#!/';
				}
		});
        
            $scope.form = [];
            $scope.files = [];            

            $scope.submit = function() {
                //alert($scope.form.bellboytip);
               // console.log($scope.files2[0]); 
	      	$scope.form.image = $scope.files[0];
            $scope.form.image2 = $scope.files2[0];
            $scope.form.image3 = $scope.files3[0];
            $scope.form.image4 = $scope.files4[0];
            $scope.form.image5 = $scope.files5[0];
            $scope.form.image6 = $scope.files6[0];
            //$scope.allImage = array($scope.form.image,$scope.form.image2,$scope.form.image3,$scope.form.image4,$scope.form.image5,$scope.form.image6);
	      	$http({
			  method  : 'POST',
			  url     : site_url+'hotel/insert_room',
			  processData: false,
			  transformRequest: function (data) {
			      var formData = new FormData();			      
                  formData.append("image1", $scope.form.image);
                  formData.append("image2", $scope.form.image2);
                  formData.append("image3", $scope.form.image3);
                  formData.append("image4", $scope.form.image4);
                  formData.append("image5", $scope.form.image5);
                  formData.append("image6", $scope.form.image6);
                  formData.append("title", $scope.form.title);
                  formData.append("description", $scope.form.room_decsription);
                  formData.append("price", $scope.form.room_price);
                  formData.append("bed", $scope.form.bed);
                  formData.append("tv", $scope.form.tv);
                  formData.append("ac", $scope.form.ac);
                  formData.append("balcony", $scope.form.balcony);                                     
                  formData.append("sea_view", $scope.form.sea_view);
                  formData.append("lat_bathe", $scope.form.lat_bathe);
                  formData.append("status", $scope.form.status);
                  formData.append("hotel_id", $scope.form.hotel);
                  formData.append("room_no", $scope.form.room_no);
                  //formData.append("allImage", $scope.allImage);
			      return formData;  
			  },  
			  data : $scope.form,
			  headers: {
			         'Content-Type': undefined
			  }
		   }).then(function(response){
		       console.log(response);
               if(response.data.error.error !== ''){
                alert(response.data.error.error);
               }
               if(response.data.success.success !== ''){
                alert(response.data.success.success);
                location.reload();
               }
		   });

	      };
          
          
           $scope.uploadedFile = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source = event.target.result;
		      $scope.$apply(function($scope) {
		        $scope.files = element.files;
		      });
		    };
                    reader.readAsDataURL(element.files[0]);
		  };
          
          $scope.files1 = [];
          
          $scope.uploadedFile1 = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source1 = event.target.result;
		      $scope.$apply(function($scope) {
		        $scope.files1 = element.files;
		      });
		    };
                    reader.readAsDataURL(element.files[0]);
		  };
          $scope.files2 = [];
          $scope.uploadedFile2 = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source2 = event.target.result;
		      $scope.$apply(function($scope) {
		        $scope.files2 = element.files;
		      });
		    };
                    reader.readAsDataURL(element.files[0]);
		  };
          $scope.files3 = [];
          $scope.uploadedFile3 = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source3 = event.target.result;
		      $scope.$apply(function($scope) {
		        $scope.files3 = element.files;
		      });
		    };
                    reader.readAsDataURL(element.files[0]);
		  };
          $scope.files4 = [];
          $scope.uploadedFile4 = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source4 = event.target.result;
		      $scope.$apply(function($scope) {
		        $scope.files4 = element.files;
		      });
		    };
                    reader.readAsDataURL(element.files[0]);
		  };
          $scope.files5 = [];
          $scope.uploadedFile5 = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source5 = event.target.result;
		      $scope.$apply(function($scope) {
		        $scope.files5 = element.files;
		      });
		    };
                    reader.readAsDataURL(element.files[0]);
		  };
          $scope.files6 = [];
          $scope.uploadedFile6 = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source6 = event.target.result;
		      $scope.$apply(function($scope) {
		        $scope.files6 = element.files;
		      });
		    };
                    reader.readAsDataURL(element.files[0]);
		  };
          
});


app.controller("bookingCtrl", function($scope,$http,DTOptionsBuilder) {
        
        // DataTables configurable options
         $scope.dtOptions = DTOptionsBuilder.newOptions()
        .withDisplayLength(10)
        .withOption('bLengthChange', false);
    
        $scope.bookingList = {};
        
		$http.post(site_url+"hotel/manage_booking")
			.then(function(response) {
				//alert(response.data);				
				$scope.bookingList = response.data.booking_list;
                $scope.site_url = site_url;
				console.log($scope.bookingList);						
			});
            
            
        $scope.change_booking = function(booking_id,status,room_id){
            //alert(booking_id);
             if (confirm("Are you sure want to change status!") === true) {
                    //alert(img);
        
                        $http.post(site_url+"hotel/change_booking_status",{
                        'status':status,
                        'booking_id':booking_id,
                        'room_id':room_id
                        })
                        .then(function(response) {
                            console.log(response.data);				
                            location.reload();  	
                        });
   
                } else {
                    
                }
        };
        
    
    $http.get(site_url+"home/check_session")
    .then(function(response) {
		$scope.authenticate = response.data.isLogin;
			if($scope.authenticate === 0){
				window.location.href = '#!/';
			}
    });
	
	
});

