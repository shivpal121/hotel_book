
app.controller('homectrl', function($scope,$http) {		
	var marks = [];
	var infoWinds = [];	
	
	$http.get(site_url+"home/featured_hotel_rooms")
    .then(function(response) {
		//console.log(response.data.content.welcome_message);
        $scope.featured_hotel = response.data.featured_rooms;
		$scope.contents = response.data.content;		
    });
	
	//get map lat long
	$http.get(site_url+"home/get_lat_long")
    .then(function(response) {	
		for(var j=0; j<response.data.total_hotels.length; j++){
			var id = response.data.total_hotels[j].id;
			var nam = response.data.total_hotels[j].name;
			var desc = response.data.total_hotels[j].description;
			var lati = response.data.total_hotels[j].latitude;
			var longi = response.data.total_hotels[j].longitude;
			var descript = desc.substring(0,100);
			//console.log(response.data.total_hotels[j].name);
			mark = [nam, lati,longi];
			marks.push(mark);
			infoWind = ['<div class="info_content"><h3><a href="#!/hotel_room_list/'+id+'">'+nam+'</a></h3><p>'+descript+' </p></div>'];						
			infoWinds.push(infoWind);
		}
		$scope.map = marks;		
		$scope.infoWindow = infoWinds;
    });
	
	
	//console.log($scope.map);
	$http.get(site_url+"home/check_session")
    .then(function(response) {
		//console.log(response.data);
        $scope.authenticate = response.data.isLogin;
			if($scope.authenticate === 0){
				window.location.href = '/';
			}				
    });
	
	$scope.get_room_detail = function(id){
		alert(id);
		$scope.titles = "Room Detail";
		$http.get(site_url+"hotel/get_room_detail/"+id)
		.then(function(response) {
			//console.log(response.data);
			$scope.roomDetail = response.data.room_detail;
			console.log($scope.roomDetail);
		});
	};
	
	$http.get(site_url+"home/get_gallery_images")
			.then(function(response) {
				console.log(response.data);
				$scope.galleryList = response.data.gallery_list;
				//$scope.contents = response.data.content;	
			});	
	
	$scope.random = function() {
        return 0.5 - Math.random();
    };
});

