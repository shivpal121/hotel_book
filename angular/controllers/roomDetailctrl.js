

app.controller('roomDetailctrl', function($scope,$http,$routeParams) {	
    $scope.title = 'Detail';	
	
		var id = $routeParams.roomId;
		$scope.titles = "Room Detail";
		$http.get(site_url+"hotel/get_room_detail/"+id)
		.then(function(response) {
			//console.log(response.data);
			$scope.Detail = response.data.room_detail;
			console.log($scope.Detail);
			//console.log($state.this);
			$scope.overview = 1;
		});
		
		$scope.get_overview = function(){
			//alert('111');			
			$scope.facilities = 0;
			$scope.extras = 0;
			$scope.overview = 1;
		};
		
		$scope.get_facilities = function(){
			//alert('111');
			$scope.overview = 0;
			$scope.extras = 0;
			$scope.facilities = 1;
		};
		
		$scope.get_extras = function(){
			//alert('111');
			$scope.overview = 0;
			$scope.facilities = 0;
			$scope.extras = 1;			
		};
		
		$http.get(site_url+"hotel/related_rooms/"+id)
		.then(function(response) {
			console.log(response.data);
			$scope.otherRoom = response.data.other_room_list;
			$scope.contents = response.data.content;
		});
		
		$scope.booking = function(){
			//alert('confirm');
		};
   
});

