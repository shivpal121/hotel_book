
app.controller('menuctrl', function($scope,$http,$interval) {	
	$http.get(site_url+"home/get_menu_names")
    .then(function(response) {		    
		$scope.menus = response.data.menus;
		$scope.language = response.data.current_lang;
		$scope.lasturi = window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
		$interval(function () {
			$scope.lasturi = window.location.href.substr(window.location.href.lastIndexOf('/') + 1);
		}, 1000);
		console.log($scope.lasturi);
    });
	
	$scope.logout = function () {					
		$http.post(site_url+"home/logout")
			.then(function(response) {
				if(response){
				alert("You have been logout successfully");
				location.reload();
				}
			},function(error){
                    alert("Sorry! Something wrong happened, please try again!");
                    console.error(error);
            });
		      
    };
   
});