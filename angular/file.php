<!DOCTYPE html>
<html>
<head>
  <title>Angularjs Image Uploading</title>
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<body>
</head>
<body ng-app="main-App" ng-controller="AdminController">

	<!-- Form Start -->
	<form ng-submit="submit()" name="form" role="form">
	
	<label ng-repeat="list in lists">
    <input type="checkbox" ng-model="active" ng-change="change(list, active)" >
</label>
     <input type="text" name="name" ng-model="form.name">
	  <input type="hidden" name="uid" ng-model="uid" value="12">
	  <input ng-model="form.image" type="file" class="form-control input-lg" accept="image/*" onchange="angular.element(this).scope().uploadedFile(this)" style="width:400px" >
	  <input type="submit" id="submit" value="Submit" />
	  <br/>
	  <img ng-src="{{image_source}}" style="width:300px;">

	</form>
	<!-- Form End -->

	<script type="text/javascript">

	    var app =  angular.module('main-App',[]);

	    app.controller('AdminController', function($scope, $http) {

	      $scope.form = [];
	      $scope.files = [];
		  
		  $scope.lists = [
        {'vl' : 1},
        {'vl' : 2},
        {'vl' : 3},
        {'vl' : 4},
        {'vl' : 5},
    ];

	$scope.change = function(list, active){
    if (active)
        $scope.lst.push(list);
    else
        $scope.lst.splice($scope.lst.indexOf(list), 1);
};

	      $scope.submit = function() {
	      	$scope.form.image = $scope.files[0];
alert($scope.active);
	      	$http({
			  method  : 'POST',
			  url     : 'http://localhost/test/angular/upload.php',
			  processData: false,
			  transformRequest: function (data) {
			      var formData = new FormData();
			      formData.append("image", $scope.form.image);
                  formData.append("name", $scope.form.name);  				  
			      return formData;  
			  },  
			  data : $scope.form,
			  headers: {
			         'Content-Type': undefined
			  }
		   }).success(function(data){
		        alert(data);
		   });

	      };

	      $scope.uploadedFile = function(element) {
		    $scope.currentFile = element.files[0];
		    var reader = new FileReader();

		    reader.onload = function(event) {
		      $scope.image_source = event.target.result
		      $scope.$apply(function($scope) {
		        $scope.files = element.files;
		      });
		    }
                    reader.readAsDataURL(element.files[0]);
		  }

	    });
	</script>

</body>
</html>